//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/extension/paraview/appcomponents/pqSMTKTaskPanel.h"

#include "smtk/extension/qt/task/qtTaskEditor.h"

#include "smtk/extension/paraview/appcomponents/ApplicationConfiguration.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"

#include "smtk/extension/qt/qtDescriptivePhraseModel.h"

#include "smtk/io/Logger.h"

#include "smtk/view/json/jsonView.h"

#include "pqApplicationCore.h"

#include <QAction>
#include <QActionGroup>
#include <QDockWidget>
#include <QLabel>
#include <QLayout>
#include <QToolBar>

pqSMTKTaskPanel::pqSMTKTaskPanel(QWidget* parent)
  : Superclass(parent)
{
  this->setObjectName("pqSMTKTaskPanel");
  auto* pqCore = pqApplicationCore::instance();
  if (pqCore)
  {
    pqCore->registerManager("smtk task panel", this);
  }

  // Either we get the application's configuration or we use a default
  // until the application's configuration plugin is loaded.
  bool immediatelyConfigured = false;
  smtk::paraview::ApplicationConfiguration::notify(
    [this, &immediatelyConfigured](smtk::paraview::ApplicationConfiguration& configurator) {
      auto viewInfo = configurator.panelConfiguration(this);
      // Extract just the view configuration.
      auto viewConfig = viewInfo.get<smtk::view::ConfigurationPtr>();
      if (viewConfig)
      {
        this->setView(viewConfig);
        immediatelyConfigured = true;
      }
    });
  if (!immediatelyConfigured)
  {
    // Parse a json representation of our default config, and use it
    // since the application can't immediately configure us.
    smtk::view::ConfigurationPtr config = smtk::extension::qtTaskEditor::defaultConfiguration();
    this->setView(config);
  }

  auto* smtkBehavior = pqSMTKBehavior::instance();
  // Now listen for future connections.
  QObject::connect(
    smtkBehavior,
    SIGNAL(addedManagerOnServer(pqSMTKWrapper*, pqServer*)),
    this,
    SLOT(resourceManagerAdded(pqSMTKWrapper*, pqServer*)));
  QObject::connect(
    smtkBehavior,
    SIGNAL(removingManagerFromServer(pqSMTKWrapper*, pqServer*)),
    this,
    SLOT(resourceManagerRemoved(pqSMTKWrapper*, pqServer*)));
}

pqSMTKTaskPanel::~pqSMTKTaskPanel()
{
  auto* pqCore = pqApplicationCore::instance();
  if (pqCore)
  {
    pqCore->unRegisterManager("smtk task panel");
  }
  delete m_viewUIMgr;
  // m_viewUIMgr deletes m_taskEditor
  // deletion of m_taskEditor->widget() is handled when parent widget is deleted.
}

void pqSMTKTaskPanel::setView(const smtk::view::ConfigurationPtr& view)
{
  m_view = view;

  auto* smtkBehavior = pqSMTKBehavior::instance();

  smtkBehavior->visitResourceManagersOnServers([this](pqSMTKWrapper* r, pqServer* s) {
    this->resourceManagerAdded(r, s);
    return false;
  });
}

void pqSMTKTaskPanel::resourceManagerAdded(pqSMTKWrapper* wrapper, pqServer* server)
{
  if (!wrapper || !server)
  {
    return;
  }
  Q_EMIT this->titleChanged("Tasks");

  smtk::resource::ManagerPtr rsrcMgr = wrapper->smtkResourceManager();
  smtk::view::ManagerPtr viewMgr = wrapper->smtkViewManager();
  if (!rsrcMgr || !viewMgr)
  {
    return;
  }
  if (m_viewUIMgr)
  {
    delete m_viewUIMgr;
    m_viewUIMgr = nullptr;
    // m_viewUIMgr deletes m_taskEditor, which deletes the container, which deletes child QWidgets.
    m_taskEditor = nullptr;
  }

  // Add the panel to the View Manger
  viewMgr->elementStateMap()[this->elementType()] = this;

  m_viewUIMgr = new smtk::extension::qtUIManager(rsrcMgr, viewMgr);
  m_viewUIMgr->setOperationManager(wrapper->smtkOperationManager());
  m_viewUIMgr->setSelection(wrapper->smtkSelection());
  // m_viewUIMgr->setSelectionBit(1);               // ToDo: should be set ?

  smtk::view::Information resinfo;
  resinfo.insert<smtk::view::ConfigurationPtr>(m_view);
  resinfo.insert<QWidget*>(this);
  resinfo.insert<smtk::extension::qtUIManager*>(m_viewUIMgr);
  resinfo.insert(wrapper->smtkManagersPtr());

  // the top-level "Type" in m_view should be qtTaskEditor or compatible.
  auto* baseView = m_viewUIMgr->setSMTKView(resinfo);
  m_taskEditor = dynamic_cast<smtk::extension::qtTaskEditor*>(baseView);
  if (baseView && !m_taskEditor)
  {
    smtkErrorMacro(smtk::io::Logger::instance(), "Unsupported task panel type.");
    return;
  }
  else if (!m_taskEditor)
  {
    return;
  }
  m_taskEditor->widget()->setObjectName("qtTaskView");
  std::string title;
  m_view->details().attribute("Title", title);
  if (title.empty())
  {
    title = "Resources";
  }
  this->setWindowTitle(title.c_str());
  Q_EMIT titleChanged(title.c_str());
  if (!m_layout)
  {
    m_layout = new QVBoxLayout;
    m_layout->setObjectName("Layout");
    this->setLayout(m_layout);
  }
  m_layout->addWidget(m_taskEditor->widget());
}

void pqSMTKTaskPanel::resourceManagerRemoved(pqSMTKWrapper* mgr, pqServer* server)
{
  if (!mgr || !server)
  {
    return;
  }
  smtk::view::ManagerPtr viewMgr = mgr->smtkViewManager();
  if (!viewMgr)
  {
    return;
  }

  // Remove the panel to the View Manger
  auto& stateMap = viewMgr->elementStateMap();
  auto it = stateMap.find(this->elementType());
  if (it != stateMap.end())
  {
    stateMap.erase(it);
  }
}

nlohmann::json pqSMTKTaskPanel::configuration()
{
  nlohmann::json config;
  if (m_taskEditor)
  {
    config["layout"] = m_taskEditor->configuration();
  }
  return config;
}

bool pqSMTKTaskPanel::configure(const nlohmann::json& data)
{
  if (m_taskEditor)
  {
    auto it = data.find("layout");
    if (it == data.end())
    {
      return false; // no layout information present
    }
    return m_taskEditor->configure(*it);
  }
  return false;
}
