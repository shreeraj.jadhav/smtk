Task System
-----------

The deprecated task-constructor signatures (that did not require
a reference to a task manager) have been removed. If your application
was using these methods, you must switch to versions that pass a
task manager.
